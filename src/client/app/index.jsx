import React from "react";
import { render } from "react-dom";
import LabelValueComponent from "./LabelValueComponent.jsx";
import TextComponent from "./TextComponent.jsx";
import FormComponent from "./FormComponent.jsx";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <Link to="/labelvalue/">Show LabelValueComponent</Link>
          <br />
          <Link to="/text">Show TextComponent</Link>
          <br />
          <Link to="/form">Show FormComponent</Link>
          <br />
          <br />
          <Switch>
            <Route
              path="/labelvalue"
              render={props => (
                <LabelValueComponent
                  {...props}
                  name="label"
                  value="new value"
                />
              )}
            />
            <Route
              path="/text"
              render={props => <TextComponent {...props} text="text example" />}
            />
            <Route path="/form" component={FormComponent} />
          </Switch>
        </div>
      </Router>
    );
  }
}

render(<App />, document.getElementById("app"));
