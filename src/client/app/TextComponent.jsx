import React from "react";

const TextComponent = function(props) {
  return <span>{props.text}</span>;
};

export default TextComponent;
