import React from "react";
import "./form.css";
import LabelValueComponent from "./LabelValueComponent.jsx";
import TextComponent from "./TextComponent.jsx";

class FormComponent extends React.Component {
  constructor(props) {
    super(props);

    this.validateAge = this.validateAge.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.ageHandleChange = this.ageHandleChange.bind(this);
    this.emailHandleChange = this.emailHandleChange.bind(this);
    this.nameHandleChange = this.nameHandleChange.bind(this);

    this.state = {
      age: "",
      name: "",
      email: "",
      isAgeCorrect: false,
      isEmailCorrect: false,
      submit: false
    };
  }

  validateAge(age) {
    var isValid = false;
    age = parseFloat(age);

    if (isNaN(age) === false && Number.isInteger(age) === true && age >= 0) {
      isValid = true;
    }

    return isValid;
  }

  validateEmail(email) {
    var pat = /^[\w\d!#$%&'*+-/=?^_`{|}~.]+@[\w\d-]+\.[\w\d]+$/;
    var isValid = false;

    if (pat.test(email) === true) {
      isValid = true;
    }

    return isValid;
  }

  nameHandleChange(event) {
    this.setState({ name: event.target.value });
  }

  ageHandleChange(event) {
    var age = event.target.value;

    if (this.validateAge(age)) {
      this.setState({ age, isAgeCorrect: true });
    } else {
      this.setState({ age, isAgeCorrect: false });
    }
  }

  emailHandleChange(event) {
    var email = event.target.value;

    if (this.validateEmail(email)) {
      this.setState({ email, isEmailCorrect: true });
    } else {
      this.setState({ email, isEmailCorrect: false });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.state.isAgeCorrect && this.state.isEmailCorrect) {
      this.setState({ submit: true });
    } else {
      this.setState({ submit: false });
    }
  }

  render() {
    let ageWarning = "";
    let emailWarning = "";

    if (this.state.submit === true) {
      return (
        <SubmitOverview
          name={this.state.name}
          age={this.state.age}
          email={this.state.email}
        />
      );
    }

    if (this.state.isAgeCorrect === false && this.state.age) {
      ageWarning = (
        <WarningComment text="age need to be positive integer value" />
      );
    }

    if (this.state.isEmailCorrect === false && this.state.email) {
      emailWarning = <WarningComment text="email is not valid" />;
    }

    return (
      <div>
        <h1>User Form</h1>
        <form onSubmit={this.handleSubmit}>
          Name:
          <input
            type="text"
            name="name"
            value={this.state.name}
            onChange={this.nameHandleChange}
          />
          <div className="comment" />
          <br />
          Age:
          <input
            type="text"
            name="age"
            value={this.state.age}
            onChange={this.ageHandleChange}
          />
          <div className="comment">{ageWarning}</div>
          <br />
          E-mail:
          <input
            type="text"
            name="email"
            value={this.state.email}
            onChange={this.emailHandleChange}
          />
          <div className="comment">{emailWarning}</div>
          <br />
          <input type="submit" className="addButton" value="Submit" />
        </form>
        <br />
        name={this.state.name}; age={this.state.age}; email={this.state.email}
      </div>
    );
  }
}

const WarningComment = props => {
  return (
    <div>
      <TextComponent text={props.text} />
      <br />
      <br />
    </div>
  );
};

const SubmitOverview = props => {
  return (
    <div>
      <h1>Submitted Values</h1>
      <LabelValueComponent name="name" value={props.name} />
      <br />
      <LabelValueComponent name="age" value={props.age} />
      <br />
      <LabelValueComponent name="e-mail" value={props.email} />
    </div>
  );
};

export default FormComponent;
