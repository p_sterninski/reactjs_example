import React from "react";

const LabelValueComponent = function(props) {
  return (
    <span>
      {props.name}: {props.value}
    </span>
  );
};

export default LabelValueComponent;
